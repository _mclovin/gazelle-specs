#!/usr/bin/env ruby

require "json"
require "yaml"
require "faraday"
require "faraday/multipart"
require "faraday-cookie_jar"
require "htmlentities"

if ARGV.empty?
  print "Please enter a permalink (e.g. https://redacted.sh/torrents.php?torrentid=4429870): "
  arg = STDIN.gets.chomp
else
  arg = ARGV.first
end

begin
  web_url, torrent_id = arg.match(/(https:\/\/[[:alpha:]\.]+)\/torrents\.php.*torrentid=([[:digit:]]+)/)[1..2]
rescue NoMethodError
  abort "Usage: ./gazelle-specs.rb \"https://gazelle.tld/torrents.php?torrentid=<torrent_id>\""
end

config = YAML::load_file('./specs.yaml')

ZOOMS = config["zooms"]
VERBOSE = config["verbose"]
FLAC_DIR  = config["flacdir"]
GAZELLE_COOKIE = config["cookies"][web_url[/\/\/([[:alnum:]]+)/, 1]]
GAZELLE_KEY = config["keys"][web_url[/\/\/([[:alnum:]]+)/, 1]]
PTPIMG_API_KEY = config["ptpimg_api_key"]
MAX_SPECS = Integer(config["max_specs"])

class GazelleAPI
  APIError     = Class.new StandardError
  AuthError    = Class.new StandardError
  PostError    = Class.new StandardError

  attr_reader :connection

  def initialize(tracker)
    @connection = Faraday.new(url: tracker) do |faraday|
      faraday.use :cookie_jar
      faraday.request :multipart
      faraday.request :url_encoded
      faraday.options.params_encoder = Faraday::FlatParamsEncoder
      faraday.adapter :net_http
    end
  end

  def authenticated?
    @authenticated
  end

  # TODO: remove key_available? when OPS gets API key support - all requests will be using keys at that point
  def key_available?
    @key_available
  end

  def fetch(resource, parameters = {})
    unless authenticated?
      raise AuthError
    end

    res = connection.get "/ajax.php", parameters.merge(:action => resource)

    if res.status == 302 && res["location"] == "login.php"
      raise AuthError, connection.host, res.status
    elsif !res.success?
      raise APIError, connection.host, res.status
    end

    parsed_res = JSON.parse res.body

    if parsed_res["status"] == "failure"
      raise APIError, parsed_res["error"]
    end

    parsed_res["response"]
  end

  # TODO: remove set_cookie when OPS gets API key support
  def set_cookie(cookie)
    connection.headers["Cookie"] = cookie
    @authenticated = true
  end

  def set_api_key(key)
    connection.headers["Authorization"] = key
    @key_available = true # TODO: remove key_available when OPS gets API key support - all requests will be using keys at that point
    @authenticated = true
  end

  def post(resource, parameters = {})
    unless authenticated?
      raise AuthError
    end

    res = connection.post "/ajax.php", parameters.merge(:action => resource)

    if res.status == 302 && res["location"] == "login.php"
      raise AuthError
    end

    parsed_res = JSON.parse res.body

    if parsed_res["status"] == "failure" || parsed_res["status"] == 400
      raise PostError.new "#{parsed_res["error"]}"
    end
    parsed_res["response"]
  end

  def update_release(web_url, payload, torrent_id)
    unless authenticated?
      raise WhatCD::AuthError
    end

    # TODO: remove path split when OPS gets API key support
    path = ""
    if key_available?
      path = "#{web_url}/ajax.php?action=torrentedit&id=#{torrent_id}"
    else
      path = "#{web_url}/torrents.php?action=edit&id=#{torrent_id}"
    end
    res = connection.post path, payload

    # TODO: remove response status split when OPS gets API key support
    if key_available?
      unless res.status == 200
        raise APIError, res.status
      end
    else
      unless res.status == 302 && res.headers["location"] =~ /torrents/
        raise APIError, res.status
      end
    end
  end
end

def ptpimg_upload(file, ptpimg_api_key)
  conn = Faraday.new(url: "https://ptpimg.me") do |f|
    f.request :multipart
    f.request :url_encoded
    f.adapter :net_http
  end
  faraFile = Faraday::UploadIO.new(file, 'image/jpeg')
  response = conn.post do |req|
    req.url '/upload.php'
    req.options.timeout = 15
    req.options.open_timeout = 15
    req.body = {
      "file-upload[0]": faraFile,
      "api_key": ptpimg_api_key
    }
  end
  faraFile.close
  if response.status == 200
    respbody = JSON.parse(response.body).first
    return "https://ptpimg.me/#{respbody["code"]}.#{respbody["ext"]}"
  else
    abort "ABORT: Could not upload #{file} to PTP, HTTP response status: #{response.status}"
  end
end

api = GazelleAPI.new(web_url)
# TODO: remove GAZELLE_COOKIE when OPS gets API key support
if GAZELLE_COOKIE
  api.set_cookie GAZELLE_COOKIE
end
if GAZELLE_KEY
  api.set_api_key GAZELLE_KEY
end
if !api.authenticated?
  # TODO: remove "cookie/key" verbiage when OPS gets API key support
  abort "ERROR: Please specify a valid cookie/key for '#{web_url[/\/\/([[:alnum:]]+)/, 1]}' in specs.yaml."
end

authkey  = api.fetch(:index)["authkey"]
torrent  = api.fetch :torrent, id: torrent_id
details  = torrent["torrent"]
fpath    = HTMLEntities.new.decode(torrent["torrent"]["filePath"]).gsub(/\u200E+/, "")
srcdir   = "#{FLAC_DIR}/#{fpath}"

unless details['encoding'].include? "Lossless"
  abort "ABORT: Not a FLAC."
end

if details['description'].downcase.include? "spectro"
  abort "ABORT: This torrent (#{fpath}) may already have spectrals in its release description."
end

unless File.directory? srcdir
  abort "Directory (#{srcdir}) not found; no point in generating spectrals."
end

spectrograms = ["[hide=Spectrograms]"]
Dir.chdir("#{srcdir}") do
  puts "Generating spectrals - #{fpath}:"
  system("mkdir", "-p", "spectrals") || abort("ABORT: Unable to create spectrals dir.")
  i = 0
  Dir.glob("./**/*.flac").sort.each do |file|
    i += 1
    break if i > MAX_SPECS && MAX_SPECS != 0
    fork do
      output = "spectrals/#{File.basename(file, ".flac")}.png"
      title = File.expand_path(file).gsub(FLAC_DIR + "/", "")
      system("sox", file, "-n", "remix", "1", "spectrogram", "-x", "3000", "-y", "513", "-z", "120", "-w", "Kaiser", "-t", title, "-o", output)
      system("oxipng", "-q", "-o", "max", "--strip", "safe", "-a", "-i0", output)
      if ZOOMS
        output = "spectrals/#{File.basename(file, ".flac")}-zoomed.png"
        system("sox", file, "-n", "remix", "1", "spectrogram", "-x", "500", "-y", "1025", "-z", "120", "-w", "Kaiser", "-S", "0:25", "-d", "0:03", "-t", "#{title} - Zoomed", "-o", output)
        system("oxipng", "-q", "-o", "max", "--strip", "safe", "-a", "-i0", output)
      end
    end
  end
  Process.waitall
  puts "Uploading to ptpimg"
  i = 0
  Dir.glob("./**/*.flac").sort.each do |file|
    i += 1
    break if i > MAX_SPECS && MAX_SPECS != 0
    output = "spectrals/#{File.basename(file, ".flac")}.png"
    title = File.expand_path(file).gsub(FLAC_DIR + "/", "")
    ptpimg_link = ptpimg_upload(File.expand_path(output), PTPIMG_API_KEY)
    puts "#{ptpimg_link}"
    spectrograms.push("[url=#{ptpimg_link}]#{title}[/url]")
    if ZOOMS
      output = "spectrals/#{File.basename(file, ".flac")}-zoomed.png"
      ptpimg_link = ptpimg_upload(File.expand_path(output), PTPIMG_API_KEY)
      spectrograms.push("[url=#{ptpimg_link}]#{title} - Zoomed[/url]")
    end
  end
  spectrograms.push("[/hide]")
  system("rm", "-r", "spectrals")
end

if VERBOSE
  puts "---\n" + spectrograms.join("\n")
end


# TODO: remove payload split when OPS gets API key support
if api.key_available?
  payload = {
    release_desc: "#{details['description']}\n#{spectrograms[0..1].join + "\n" + spectrograms[2..-1].join("\n")}",
  }
else
  payload = {
    auth: authkey,
    action: "takeedit",
    torrentid: torrent_id,
    groupid: torrent['group']['id'],
    release_desc: "#{details['description']}\n#{spectrograms[0..1].join + "\n" + spectrograms[2..-1].join("\n")}",
    remaster_title: details['remasterTitle'],
    remaster_year: details['remasterYear'],
    remaster_record_label: details['remasterRecordLabel'],
    remaster_catalogue_number: details['remasterCatalogueNumber'],
    media: details['media'],
    format: details['format'],
    bitrate: details['encoding'],
    groupremasters: 0,
    type: 1,
    submit: "true"
  }
end

print "---\nAdding spectrograms to release description ... "
api.update_release(web_url, payload, torrent_id)
puts "done."
